### Language
* [ ] C#
* [ ] Java
* [ ] Python 2.7
* [ ] Python 3  

### AltUnityTester Version
* [ ] 0.2.3
* [ ] 1.2.0
* [ ] 1.2.1

### Summary

(Summarize the bug encountered concisely)

### Steps to reproduce

(How one can reproduce the issue - this is very important)


### What is the expected *correct* behavior?

(What you should see instead)

### Relevant logs and/or screenshots

(Paste any relevant logs - please use code blocks (```) to format console output,
logs, and code as it's very hard to read otherwise.)

